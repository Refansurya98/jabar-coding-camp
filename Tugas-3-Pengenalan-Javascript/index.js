// soal 1
var kalimat1 = 'saya sangat senang hari ini';
var kalimat2 = 'belajar javascript itu keren';
var car3 = kalimat1.substring(0,4);
var car4 = kalimat1.substring(11,18);
var car5 = kalimat2.substring(0,7);
var car6 = kalimat2.substring(8,18);
var upper = car6.toUpperCase();
console.log(car3+car4+(' ')+car5+(' ')+upper);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
console.log((kataPertama%kataKedua)+(kataKetiga*kataKeempat));

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);